package ee.ut.university.indoorpositioning;


/**
 * Created by thorben on 03/10/15.
 */
public class IPScanResult {
    private String bssid;
    private int level;

    public IPScanResult(String bssid, int level) {
        this.bssid = bssid;
        this.level = level;
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean equals(IPScanResult sr) {
        if(this.bssid.equals(sr.getBssid()) &&
                this.level == sr.getLevel()) {

            return true;
        }

        return false;
    }
}
