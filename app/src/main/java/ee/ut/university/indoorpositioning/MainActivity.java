package ee.ut.university.indoorpositioning;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;


public class MainActivity extends Activity {
    GraphView graphView;

    HashMap<String, DataPoint[]> levels = new HashMap<String, DataPoint[]>();
    int scanID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        graphView = (GraphView) findViewById(R.id.graph);
        graphView.getViewport().setYAxisBoundsManual(true);
        graphView.getViewport().setMinY(30);
        graphView.getViewport().setMaxY(100);

        scanID = 0;

        registerReceiver(new WifiScanReceiver(), new IntentFilter("ee.ut.university.indoorpositioning.newScanResults"));

        Intent intent = new Intent(this, WifiScanner.class);
        startService(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private class WifiScanReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context c, Intent intent) {
            try {
                JSONObject scanResults = new JSONObject(intent.getStringExtra("data"));
                JSONArray routers = scanResults.getJSONArray("routers");

                graphView.removeAllSeries();
                for (int index = 0; index < routers.length(); index++) {
                    JSONObject sr = (JSONObject) routers.get(index);
                    String bssid = sr.getString("bssid");
                    int level = sr.getInt("level");

                    if (!levels.containsKey(bssid)) {
                        levels.put(bssid, new DataPoint[30]);
                        for (int i = 0; i < 30; i++) {
                            levels.get(bssid)[i] = new DataPoint(i, 0);
                        }
                    }

                    levels.get(bssid)[scanID] = new DataPoint(scanID, level);
                    for (int i = scanID + 1; i < 30; i++) {
                        levels.get(bssid)[i] = levels.get(bssid)[scanID];
                    }

                    graphView.addSeries(new LineGraphSeries<DataPoint>(levels.get(bssid)));
                }
                scanID++;
                if(scanID == 30) {
                    levels.clear();
                    scanID = 0;
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
