package ee.ut.university.indoorpositioning;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.widget.ImageView;

import java.util.Vector;

/**
 * Created by thorben on 27/09/15.
 */
public class IPImageView extends ImageView {
    private Paint paintLastClick;
    private Paint paintPermantentPoint;
    private Paint paintCalibrationPoint;

    private int touchX, touchY;

    private int permantentPointsWidth;
    private Vector<Point> permantentPoints;

    private int calibrationPointsWidth;
    private Vector<Point> calibrationPoints;


    public IPImageView(Context context, AttributeSet attrs) {
        super(context, attrs);

        paintLastClick = new Paint();
        paintLastClick.setColor(Color.BLUE);

        paintPermantentPoint = new Paint();
        paintPermantentPoint.setColor(Color.GREEN);

        paintCalibrationPoint = new Paint();
        paintCalibrationPoint.setColor(Color.RED);

        permantentPointsWidth = 2;
        calibrationPointsWidth = 24;

        touchX = touchY = 10;
        permantentPoints = new Vector<>();
        calibrationPoints = new Vector<>();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for(Point p : permantentPoints) {
            canvas.drawCircle(p.x, p.y, permantentPointsWidth, paintPermantentPoint);
        }
        for(Point p : calibrationPoints) {
            canvas.drawCircle(p.x - calibrationPointsWidth/2, p.y - calibrationPointsWidth/2, calibrationPointsWidth, paintCalibrationPoint);
        }

        canvas.drawCircle(touchX-5, touchY-5, 10, paintLastClick);
    }

    public void touch(int x, int y, boolean permantent) {
        touchX = x;
        touchY = y;

        if(permantent) {
            permantentPoints.add(new Point(x, y));
        }
    }

    public void clearPermanentPoints() {
        permantentPoints.clear();
    }

    public void addCalibrationPoint(Point p) {
        if(!calibrationPoints.contains(p)) {
            calibrationPoints.add(p);
        }
    }

    public void clearCalibrationPoints() {
        calibrationPoints.clear();
    }
}
