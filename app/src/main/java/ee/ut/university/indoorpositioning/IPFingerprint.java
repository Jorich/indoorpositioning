package ee.ut.university.indoorpositioning;

import android.graphics.Point;
import android.net.wifi.ScanResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Vector;

/**
 * Created by thorben on 03/10/15.
 */
public class IPFingerprint {
    private Vector<IPScanResult> scans;
    private Date timestamp;
    private String type;
    private int siteId;
    private int pathId;
    private Point position;

    public IPFingerprint(int siteId, int pathId, Date timestamp) {
        this.timestamp = timestamp;
        scans = new Vector<>();
        this.siteId = siteId;
        this.pathId = pathId;
    }

    public void addScanResult(IPScanResult sR) {
        if(!this.scans.contains(sR)) {
            this.scans.add(sR);
        }
    }

    public Vector<IPScanResult> getScans() {
        return scans;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getPathId() {
        return pathId;
    }

    public void setPathId(int pathId) {
        this.pathId = pathId;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public JSONObject toJson() {
        JSONObject data = new JSONObject();
        JSONArray routers = new JSONArray();

        try {
            for(IPScanResult sr : scans) {
                JSONObject ap = new JSONObject();
                ap.put("bssid", sr.getBssid());
                ap.put("level", sr.getLevel());
                routers.put(ap);
            }
            data.put("routers", routers);
        }
        catch(JSONException e) {
            e.printStackTrace();
        }

        return data;
    }
}
