package ee.ut.university.indoorpositioning;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.ByteBuffer;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;



public class ImageViewActivity extends Activity {
    Bitmap mBitmap;
    int bitmapWidth;
    int bitmapHeight;

    MapPoint start, destination;

    int[][] map = null;
    Vector<Point> corners;

    Point lastClick;
    Point calibrationPoint;
    int currentPathId;
    private int currentSiteId;

    boolean liveMeasurementInProgress;

    IPImageView imgView;
    Button btnCalibrate;
    Button btnStart;
    ProgressBar pbCalibrationProgress;
    WifiScanReceiver wifiScanReceiver;
    Http http;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        currentPathId = 0;
        currentSiteId = 1;
        liveMeasurementInProgress = false;

        corners = new Vector<>();
        initializeMapView();

        Button btnToWifiScan  = (Button) findViewById(R.id.btnToWifiScan);
        btnToWifiScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ImageViewActivity.this, MainActivity.class));
            }
        });

        http = new Http();
        wifiScanReceiver = new WifiScanReceiver();
        registerReceiver(wifiScanReceiver, new IntentFilter("ee.ut.university.indoorpositioning.newScanResults"));

        pbCalibrationProgress = (ProgressBar) findViewById(R.id.pbCalibrationProgress);

        btnCalibrate  = (Button) findViewById(R.id.btnCalibrate);
        btnCalibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(lastClick != null) {
                    btnCalibrate.setEnabled(false);
                    btnStart.setEnabled(false);
                    wifiScanReceiver.precision = 10;
                    calibrationPoint = lastClick;
                }
            }
        });

        btnStart = (Button) findViewById(R.id.btnStart);
        btnStart.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(!liveMeasurementInProgress) {
                    btnCalibrate.setEnabled(false);
                    btnStart.setEnabled(false);
                    http.createNewPath(ImageViewActivity.this, currentSiteId);
                }
                else {
                    liveMeasurementInProgress = false;
                    wifiScanReceiver.precision = 0;
                    btnCalibrate.setEnabled(true);
                    btnStart.setText("Start");
                }
            }
        });

        start = null;
        destination = null;

        wifiScanReceiver.precision = 0;
        startService(new Intent(ImageViewActivity.this, WifiScanner.class));
    }



    private class WifiScanReceiver extends BroadcastReceiver {
        private Vector<IPFingerprint> scans = new Vector<>();
        public int precision = 0;

        @Override
        public void onReceive(Context c, Intent intent) {
                try {
                    JSONObject scanResults = new JSONObject(intent.getStringExtra("data"));
                    JSONArray routers = scanResults.getJSONArray("routers");

                    IPFingerprint newFingerprint = new IPFingerprint(currentSiteId, currentPathId, new Date(System.currentTimeMillis()));

                    for (int index = 0; index < routers.length(); index++) {
                        JSONObject sr = (JSONObject) routers.get(index);
                        String bssid = sr.getString("bssid");
                        int level = sr.getInt("level");

                        newFingerprint.addScanResult(new IPScanResult(bssid, level));
                    }

                    if(precision == 0) {    } //Do nothing
                    else if(precision < 0 && liveMeasurementInProgress){ //Live Measurements
                        newFingerprint.setType("live");
                        newFingerprint.setPosition(new Point(-1, -1));
                        http.postPoint(ImageViewActivity.this, newFingerprint);
                    }
                    else if (scans.size() == precision) { // Calibration finished
                        IPFingerprint calibrationFingerprint = createCalibrationPoint();

                        http.postPoint(ImageViewActivity.this, calibrationFingerprint);
                        btnCalibrate.setEnabled(true);
                        btnStart.setEnabled(true);
                        scans.clear();
                        precision = 0;

                        imgView.addCalibrationPoint(calibrationPoint);
                        imgView.invalidate();
                    }
                    else { //Calibration in process
                        newFingerprint.setPosition(calibrationPoint);
                        newFingerprint.setType("calibration");
                        scans.add(newFingerprint);
                        pbCalibrationProgress.setProgress((100 / precision) * scans.size());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

        private IPFingerprint createCalibrationPoint() {
            HashMap<String, Integer> averages = new HashMap<>();
            HashMap<String, Integer> counts = new HashMap<>();

            for(IPFingerprint fP : scans) {
                for(IPScanResult sR : fP.getScans()) {
                    if(!averages.containsKey(sR.getBssid())) {
                        averages.put(sR.getBssid(), sR.getLevel());
                        counts.put(sR.getBssid(), 1);
                    }
                    else {
                        averages.put(sR.getBssid(), averages.get(sR.getBssid()) + sR.getLevel());
                        counts.put(sR.getBssid(), counts.get(sR.getBssid()) + 1);
                    }
                }
            }

            IPFingerprint averageFP = new IPFingerprint(currentSiteId, -1, scans.lastElement().getTimestamp());
            averageFP.setPosition(calibrationPoint);
            averageFP.setType("calibration");
            for(Map.Entry<String, Integer> entry : averages.entrySet()) {
                int a = (int) entry.getValue() / counts.get(entry.getKey());
                averageFP.addScanResult(new IPScanResult(entry.getKey(), a));
            }

            return averageFP;
        }
    }

    public void click(int x, int y) {
        lastClick = new Point(x, y);
        imgView.touch(x, y, false);

        if(x > 0 && y > 0 && x < bitmapWidth && y < bitmapHeight) {
            int gray = getGreyScaleOfPixel(x, y);
            if (map[x][y] == 0) {
                if(start == null) {
                    //start = new MapPoint(xOnImg, yOnImg, null);
                    Toast.makeText(this, x + "/" + y + " - " + gray, Toast.LENGTH_SHORT).show();
                }
                else if(destination == null) {
                    destination = new MapPoint(x, y, null);

                    //findShortestPath();
                    //start = null;
                    //destination = null;
                }
                else {
                    findShortestPath();
                }
            } else {
                Toast.makeText(this, "wall", Toast.LENGTH_SHORT).show();
            }
            imgView.invalidate();
        }else {
            Toast.makeText(this, "index out of bounds", Toast.LENGTH_SHORT).show();
        }
    }

    private void initializeMapView() {
        Resources res=getResources();

        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inTargetDensity = 0;
        mBitmap = BitmapFactory.decodeResource(res, R.drawable.floorplan, opts);

        bitmapWidth = mBitmap.getWidth();
        bitmapHeight = mBitmap.getHeight();

        imgView = (IPImageView) findViewById(R.id.ivMap);
        imgView.setImageBitmap(mBitmap);
        imgView.invalidate();
        imgView.setOnTouchListener(new ImageScrollHandler());

        map = new int[bitmapWidth][bitmapHeight];
        //createMap();
    }

    private void createMap() {
        int gray;
        imgView.clearPermanentPoints();
        for(int i = 0; i < bitmapWidth; i++) {
            for (int a = 0; a < bitmapHeight; a++) {
                gray = getGreyScaleOfPixel(i, a);
                if (gray > 170) {
                    map[i][a] = 0;
                } else if (gray < 170 && gray > 0) {
                    map[i][a] = 1;
                    imgView.touch(i, a, true);
                }
            }
        }

        int threshhold = 0;
        int lastGray = 240;
        int windowSize = 7;
        for(int i = 5; i < bitmapWidth-7; i+=6) {
            for (int a = 5; a < bitmapHeight-(windowSize+5); a+=windowSize) {
                gray = 0;
                for(int steps = 0; steps < windowSize; steps++) {
                    gray =+ getGreyScaleOfPixel(i, a+steps);
                }
                gray /= windowSize;

                if(gray + threshhold < lastGray) {
                    corners.add(new Point(i, a-4));
                    imgView.touch(i, a-4, true);
                }
                else if(gray - threshhold > lastGray) {
                    corners.add(new Point(i, a+4));
                    imgView.touch(i, a+4, true);
                }
                lastGray = gray;
            }
        }
        imgView.invalidate();
    }

    private class ImageScrollHandler implements View.OnTouchListener {
        int offsetX = 0;
        int offsetY = 0;
        float startingX = 0;
        float startingY = 0;
        float lastX = 0;
        float lastY = 0;
        @Override
        public boolean onTouch(View arg0, MotionEvent event) {
            float curX, curY;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startingX = lastX = event.getX();
                    startingY = lastY = event.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    curX = event.getX();
                    curY = event.getY();
                    imgView.scrollBy((int) (lastX - curX), (int) (lastY - curY));
                    offsetX += (int) (lastX - curX);
                    offsetY += (int) (lastY - curY);
                    lastX = curX;
                    lastY = curY;
                    break;
                case MotionEvent.ACTION_UP:
                    if(startingX - lastX > -2 && startingX - lastX < 2 &&
                            startingY - lastY > -2 && startingY - lastY < 2) {

                        click(offsetX + (int)lastX, offsetY + (int)lastY);
                    }
                    break;
            }

            return true;
        }
    }

    private int getGreyScaleOfPixel(int x, int y) {
        int color = mBitmap.getPixel(x, y);
        ByteBuffer b = ByteBuffer.allocate(4);
        b.putInt(color);
        byte[] result = b.array();
        return (int)(result[1] & 0xFF);
    }

    private void findShortestPath() {
        //Development postponed
        int step = 1;
        boolean destFound = false;
        Vector<MapPoint> knownPoints = new Vector<>();
        Vector<MapPoint> pointsToRemove = new Vector<>();
        Vector<MapPoint> pointsToAdd = new Vector<>();
        Vector<MapPoint> pointsToCheck = new Vector<>();
        MapPoint dummyPoint = new MapPoint(-1, -1, null);
        double progress = 0;

        boolean removePoint = true;
        MapPoint newPoint;

        if(!knownPoints.contains(start)) {
            knownPoints.add(start);
        }
        if(!pointsToCheck.contains(start)) {
            pointsToCheck.add(start);
        }
        for(int test = 0; test < 5; test++) {
            if(progress < -20 && step < 6) {
                step++;
            }
            else if(progress > 20 && step > 1) {
                step--;
            }
            progress = 0;
            for (int run = 0; run < 100; run++) {
                //while(!destFound) {
                //for(MapPoint currentPoint : pointsToCheck) {
                MapPoint currentPoint = pointsToCheck.elementAt(0);
                removePoint = true;

                //123
                //4X5
                //678
                if (map[currentPoint.x - step][currentPoint.y - step] == 0) { // 1
                    newPoint = new MapPoint(currentPoint.x - step, currentPoint.y - step, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }
                if (map[currentPoint.x][currentPoint.y - step] == 0) { // 2
                    newPoint = new MapPoint(currentPoint.x, currentPoint.y - step, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }
                if (map[currentPoint.x + step][currentPoint.y - step] == 0) { // 3
                    newPoint = new MapPoint(currentPoint.x + step, currentPoint.y - step, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }


                if (map[currentPoint.x - step][currentPoint.y] == 0) { // 4
                    newPoint = new MapPoint(currentPoint.x - step, currentPoint.y, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }
                if (map[currentPoint.x + step][currentPoint.y] == 0) { // 5
                    newPoint = new MapPoint(currentPoint.x + step, currentPoint.y, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }

                if (map[currentPoint.x - step][currentPoint.y + step] == 0) { // 6
                    newPoint = new MapPoint(currentPoint.x - step, currentPoint.y + step, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }
                if (map[currentPoint.x][currentPoint.y + step] == 0) { // 7
                    newPoint = new MapPoint(currentPoint.x, currentPoint.y + step, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }
                if (map[currentPoint.x + step][currentPoint.y + step] == 0) { // 8
                    newPoint = new MapPoint(currentPoint.x + step, currentPoint.y + step, currentPoint);
                    if (!knownPoints.contains(newPoint)) {
                        knownPoints.add(newPoint);

                        pointsToAdd.add(newPoint);
                        removePoint = false;
                    }
                }

                if (removePoint) {
                    pointsToRemove.add(currentPoint);
                }
                if (currentPoint.equals(destination)) {
                    destFound = true;
                }
                //}

                for (MapPoint p : pointsToAdd) {
                    pointsToCheck.add(p);
                    p.cost = Math.sqrt(Math.pow(p.x - destination.x, 2) + (Math.pow(p.y - destination.y, 2)));
                    progress += p.origin.cost - p.cost;
                    for (int i = 1; i < pointsToCheck.size(); i++) {
                        if (pointsToCheck.get(i).cost > p.cost) {
                            pointsToCheck.insertElementAt(p, i - 1);
                            break;
                        }
                    }
                    if (!pointsToCheck.contains(p)) {
                        pointsToCheck.add(p);
                    }
                }
                pointsToAdd.clear();

                for (MapPoint p : pointsToRemove) {
                    pointsToCheck.remove(p);
                }
                pointsToRemove.clear();

            }
        }
        imgView.clearPermanentPoints();
        for(Point p : pointsToCheck) {
            imgView.touch(p.x, p.y, true);
        }
        imgView.invalidate();

        if(destFound == true) {
            boolean pathFound = false;
            Vector<Point> path = new Vector<>();
            path.add(knownPoints.get(knownPoints.indexOf(destination)));
            while (!pathFound) {
                if (path.lastElement().equals(start)) {
                    break;
                }
                path.add(knownPoints.get(knownPoints.indexOf(path.lastElement())).origin);
            }

            imgView.clearPermanentPoints();
            for (Point p : path) {
                imgView.touch(p.x, p.y, true);
            }
            imgView.invalidate();
        }
    }

    private class MapPoint extends Point {
        public MapPoint origin;
        public double cost;
        public MapPoint(int x, int y, MapPoint origin) {
            super(x, y);
            this.origin = origin;
            this.cost = cost;
        }

        public boolean equals(MapPoint p) {
            if(x == p.x && y == p.y) {
                return true;
            }
            return false;
        }
    }

    public void setPrecision(int prez) {
        //Necessaty Method for precision sets outside of this class
        wifiScanReceiver.precision = prez;
    }
}
